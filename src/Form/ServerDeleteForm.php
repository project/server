<?php

namespace Drupal\server\Form;

use Drupal\Core\Entity\ContentEntityDeleteForm;

/**
 * Provides a form for deleting Server entities.
 *
 * @ingroup server
 */
class ServerDeleteForm extends ContentEntityDeleteForm {


}
